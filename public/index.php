<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;


require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

$twig = Twig::create(__DIR__ . '/../templates',
    ['cache' => false,'debug' => true]);

$app->add(TwigMiddleware::create($app, $twig));
$printSvg = new \Twig\TwigFunction('printSvg', function ($file) {
    $dir = __DIR__ . '/svg//';
    $filepath = $dir . $file . '.svg';
    return file_get_contents($filepath, true);
});

$formatPricing = new \Twig\TwigFunction('priceHtml', function ($price) {
    $price = str_split(strrev(strval($price)),3);
    $o = '<span class="currency">Rp </span>';
    $i = count($price) -1;
    while ($i >= 0) {
      $o .= '<span class="price-item">' . strrev($price[$i]) . '</span>';
      $i--;
    }
    return $o;
});

$twig->getEnvironment()->addFunction($formatPricing);
$twig->getEnvironment()->addFunction($printSvg);



$app->get('/', function (Request $request, Response $response, $args) {
    $pricingData = file_get_contents(__DIR__ . '/../resources/data/pricing.json');
    $view = Twig::fromRequest($request);
    return $view->render($response, 'index.twig', [
      'pricing' => json_decode($pricingData)
    ]);
});

$app->run();
